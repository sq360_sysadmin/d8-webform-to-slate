<?php

namespace Drupal\webform_to_slate\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformRequestInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class WebToSlateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_to_lead';
  }

  /**
   * The webform entity.
   *
   * @var \Drupal\webform\WebformInterface
   */
  protected $webform;

  /**
   * The webform source entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $sourceEntity;

  /**
   * The webform submission storage.
   *
   * @var \Drupal\webform\WebformSubmissionStorageInterface
   */
  protected $submissionStorage;

  /**
   * Webform request handler.
   *
   * @var \Drupal\webform\WebformRequestInterface
   */
  protected $requestHandler;

  /**
   * Constructs a object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\webform\WebformRequestInterface $request_handler
   *   The webform request handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, WebformRequestInterface $request_handler) {
    $this->submissionStorage = $entity_type_manager->getStorage('webform_submission');
    $this->requestHandler = $request_handler;
    list($this->webform, $this->sourceEntity) = $this->requestHandler->getWebformEntities();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('webform.request')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('webform_to_slate.forms');
    $settings = $config->get();
    $form['web2slate'] = [
      '#type' => "fieldset",
      '#title' => 'Web 2 Slate Settings'
    ];
    $form['web2slate']['active'] = array(
      '#type' => 'radios',
      '#title' => t('Submit to Slate'),
      '#default_value' => (isset($settings[$this->webform->get("id")]) ? $settings[$this->webform->get("id")]['active'] : "0"),
      '#options' => [
        0 => 'NO',
        1 => "Yes"
      ],
      '#description' => t('If yes, the form will be sent via CURL to Slate.'),
    );
    $form['web2slate']['slate_form_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Slate Form URL'),
      '#default_value' => (isset($settings[$this->webform->get("id")]) ? $settings[$this->webform->get("id")]['slate_form_url'] : ""),
    );
    $form['web2slate']['slate_form_id'] = array(
      "#type" => "textfield",
      "#title" => "Slate Form ID",
      '#default_value' => (isset($settings[$this->webform->get("id")]) ? $settings[$this->webform->get("id")]['slate_form_id'] : ""),
    );

    /* Field Mapping  ============================================================================== */
    $form['web2slate']['field_wrapper'] = [
      '#type' => "fieldset",
      '#title' => "Webform Fields to Slate Fields Mapping Table",
      "#description" => "Legend: KEY is the Webform Field Key, VALUE is the matching Slate ID for the field",
      '#attributes' => [
        'id' => 'fields-wrapper',
      ],
    ];

    $form['web2slate']['field_wrapper']['field_table'] = array(
      '#type' => 'table',
      '#header' => array(t('Webform Key'), t('Slate Field ID')),
      '#attributes' => [
        'id' => ['fields-table'],
      ],
    );
    $count = 0;

      /* print('<pre>');
      print_r($settings[$this->webform->get("id")]['fields']);
      print('</pre>');
      exit(); */

    if (isset($settings[$this->webform->get("id")]['fields'])) {
      foreach ($settings[$this->webform->get("id")]['fields'] as $i => $field) {
        foreach ($field as $key => $value) {
          $form['web2slate']['field_wrapper']['field_table'][$i] = $this->getTableLine($i, [$key, $value]);
        }
      }
      $count = count($settings[$this->webform->get("id")]['fields']);
    }

    $count++;
    $form['web2slate']['field_wrapper']['field_table'][$count] = $this->getTableLine($count);
    $count++;
    // Build the extra lines
    $triggeringElement = $form_state->getTriggeringElement();
    $clickCounter = 0;
    // if a click occurs
    if ($triggeringElement) {
      if (isset($triggeringElement['#attributes']['id']) && $triggeringElement['#attributes']['id'] == 'add-row') {
        // $formstate and $form element are updated
        // click counter is incremented
        if ($form_state->hasValue("click_counter")) {
          $clickCounter = $form_state->getValue('click_counter');
        }
        $clickCounter++;
        $form_state->setValue('click_counter', $clickCounter);
        $form['web2slate']['click_counter'] = array('#type' => 'hidden', '#default_value' => 0, '#value' => $clickCounter);
      }
    }
    else {
      $form['web2slate']['click_counter'] = array('#type' => 'hidden', '#default_value' => 0);
    }
    // Build the extra table rows and columns.
    for ($k = 0; $k < $clickCounter; $k++) {
      $form['web2slate']['field_wrapper']['field_table'][$count + $k] = $this->getTableLine($count + $k);
    }

    $form['web2slate']['field_wrapper']['add_field'] = array(
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#attributes' => array(
        'id' => 'add-row'
      ),
    );

    // END Field Mappings  =================================================================================

    $form['web2slate']['submit'] = array('#type' => 'submit', '#value' => t('Save Settings'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
     $triggeringElement = $form_state->getTriggeringElement();
    if ($triggeringElement && isset($triggeringElement['#attributes']['id']) &&
      ($triggeringElement['#attributes']['id'] == 'add-row')
    ) {
      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();

    if (isset($triggeringElement['#id']) && $triggeringElement['#id'] != "add-row") {
      $style = $form_state->getValue("state");
      $values = $form_state->getValues();
      $fields = array();

     /*  print('<pre>');
      print_r($values);
      print('</pre>');
      exit(); */

      foreach ($values['field_table'] as $value) {
        if (!empty($value['key']) && !empty($value['value'])) {
          $fields[] = [$value['key'] => $value['value']];
        }
      }


    }

    $config = \Drupal::service('config.factory')->getEditable('webform_to_slate.forms');
    $settings = [
      'slate_form_id' => $form_state->getValue('slate_form_id'),
      'active' => $form_state->getValue('active'),
      'slate_form_url' => $form_state->getValue('slate_form_url'),
      'fields' => $fields,
    ];
    $config->set($this->webform->get("id"), $settings)->save();

  }

  public function getTableLine($i, $defaultValue = []) {
    $line = array();
    $line['key'] = array(
      '#type' => 'textfield',
      '#limit' => 150,
      "#default_value" => (isset($defaultValue[0]) ? $defaultValue[0] : "")
    );
    $line['value'] = array(
      '#type' => 'textfield',
      '#limit' => 150,
      "#default_value" => (isset($defaultValue[1]) ? $defaultValue[1] : "")
    );
    return $line;
  }

}
